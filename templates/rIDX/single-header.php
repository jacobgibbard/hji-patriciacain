<?php
/**
 * Single rIDX Listing Header
 */
?>

<div id="custom-property-video"><div id="sundayskyDiv"></div></div>

<?php if ( $listing->coordinates_latitude != '' && $listing->coordinates_longitude != '' ) {
    $coords = $listing->coordinates_latitude. ':' .$listing->coordinates_longitude;
//    var_dump($coords);
} ?>

<ul id="rIDX-tabs" class="nav nav-tabs responsive">
    <li class="active"><a href="#property-details" role="tab" data-toggle="tab">Details</a></li>
    <li><a href="#property-map" role="tab" data-toggle="tab">Maps</a></li>
    <?php if ( isset( $coords ) && $coords != '' ) : ?>
        <li><a href="#school-details" role="tab" data-toggle="tab">Schools</a></li>
        <li><a href="#area-info-details" role="tab" data-toggle="tab">Area</a></li>
        <li><a href="#business-info-details" role="tab" data-toggle="tab">Businesses</a></li>
        <li><a href="#home-value-details" role="tab" data-toggle="tab">Home Values</a></li>
    <?php endif; ?>
</ul>

<div class="main-info-block tab-content responsive clearfix">

    <div id="property-details" class="tab-pane active">

        <?php require_once locate_template( '/templates/rIDX/single-photo.php' ); ?>

        <?php require_once locate_template( '/templates/rIDX/single-details.php' ); ?>

        <?php require_once locate_template( '/templates/rIDX/single-slider.php' ); ?>

    </div>

    <?php require_once locate_template( '/templates/rIDX/single-maps.php' ); ?>

    <?php if ( isset( $coords ) && $coords != '' ) : ?>

        <div id="school-details" class="tab-pane">
            <h4>Schools</h4>
            <?php echo do_shortcode( '[school_district coords="'. $coords .'"]' ); ?>
        </div>

        <div id="area-info-details" class="tab-pane">
            <h4>Area Info</h4>
            <?php echo do_shortcode( '[lifestyle_finder bookmark="/areas/do_search?where='. $listing->address_zip .'" height="900px" width="100%"]' ); ?>
        </div>

        <div id="business-info-details" class="tab-pane">
            <h4>Local Businesses</h4>
            <?php echo do_shortcode( '[lifestyle_finder bookmark="/businesses/do_search?where='. $listing->address_zip .'" height="1100px;" width="100%"]' ); ?>
        </div>

        <div id="home-value-details" class="tab-pane">
            <h4>Home Values</h4>
            <?php echo do_shortcode( '[recent_sales market="mris" coords='. $coords .']' ); ?>
        </div>

    <?php endif; ?>

</div>