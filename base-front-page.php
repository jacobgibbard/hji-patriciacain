<!DOCTYPE html>

<html <?php language_attributes(); ?>>

    <?php get_header( hji_theme_template_base() ); ?>

    <body <?php body_class(); ?>>

        <!--[if lt IE 8]>
            <div class="alert alert-warning">
                <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'hji-textdomain'); ?>
            </div>
        <![endif]-->

            <?php get_template_part( 'templates/topofthepage' ); ?>

            <div id="wrapper" class="body-wrapper">

            <?php do_action( 'hji_theme_before_navbar' ); ?>
            
            <?php get_template_part( 'templates/header-navbar' ); ?>
            
            <?php do_action( 'hji_theme_after_navbar' ); ?>

            <div class="blvd-slideshow"></div>

            <section id="primary" class="primary-wrapper container">

                <div class="row">

                    <?php do_action( 'hji_theme_before_content' ); ?>

                    <div id="content" class="<?php echo hji_theme_main_class(); ?>" role="main">

                        <?php do_action( 'hji_theme_before_content_col' ); ?>

                        <?php include hji_theme_template_path(); ?>

                        <?php do_action( 'hji_theme_after_content_col' ); ?>

                    </div>

                    <?php do_action( 'hji_theme_before_sidebar' ); ?>
                    
                    <?php ( hji_theme_display_sidebar() ? get_sidebar( hji_theme_template_base() ) : '' ) ?>

                    <?php do_action( 'hji_theme_after_sidebar' ); ?>

                </div>
            
            </section>            

            <?php do_action( 'hji_theme_after_primary' ); ?> 

            <?php if ( is_active_sidebar( 'blvd-upper-homewidgets' ) ) : ?>

                <section class="container upper-homewidgets-wrapper">

                    <div class="blvd-upper-homewidgets">
                        <?php dynamic_sidebar( 'blvd-upper-homewidgets'); ?>
                    </div>

                </section>

            <?php endif; ?>                     

            <section class="container">

                <?php get_template_part( 'templates/cta-boxes' ); ?>  
            
            </section>    

            <section class="map-wrap">
                <div class="svg-wrap" id="vectormap">
                    <img src="/wp-content/uploads/svg-mock.png" />
                </div>
                <div class="map-content">
                    <h3>Search Homes by City</h3>
                    <div class="map-links">
                        <a href="/" class="phoenix">Phoenix</a>
                        <a href="/" class="scottsdale">Scottsdale</a>
                        <a href="/" class="tempe">Tempe</a>
                        <a href="/" class="mesa">Mesa</a>
                        <a href="/" class="gilbert">Gilbert</a>
                        <a href="/" class="chandler">Chandler</a>
                        <a href="/" class="paradisevalley">Paradise Valley</a>
                        <a href="/" class="glendale">Glendale</a>
                        <a href="/" class="peoria">Peoria</a>
                    </div>
                </div>
            </section>

            <br clear="all" />
            
            <?php if ( is_active_sidebar( 'blvd-middle-homewidgets' ) ) : ?>

                <section class="container middle-homewidgets-wrapper">

                    <div class="blvd-middle-homewidgets">
                        <?php dynamic_sidebar( 'blvd-middle-homewidgets'); ?>
                    </div>

                </section>

            <?php endif; ?>     

            <section class="testimonials-slideshow-wrap">
                
                <?php putRevSlider("testimonials") ?>

            </section>                   

            <?php if ( is_active_sidebar( 'blvd-homepageparallax' ) ) : ?>

                    <div class="homepage-parallax">
                        <div class="container">
                            <div class="blvd-homepage-parallax row">
                                <?php dynamic_sidebar( 'blvd-homepageparallax'); ?>
                            </div>
                        </div>
                    </div>

            <?php endif; ?>

            <section class="container bottom-homewidgets-wrapper">

                <?php if ( is_active_sidebar( 'blvd-bottom-homewidgets' ) ) : ?>

                    <div class="blvd-bottom-homewidgets">
                        <?php dynamic_sidebar( 'blvd-bottom-homewidgets'); ?>
                    </div>

                <?php endif; ?>

            </section> 

            <?php get_footer( hji_theme_template_base() ); ?>

        </div>

    </body>

</html>